"""Miscellaneous utility functions."""

import colorsys
import os
from functools import reduce
from functools import wraps

import cv2 as cv
import keras.backend as K
import numpy as np
import tensorflow as tf
from PIL import Image
from keras.callbacks import TensorBoard, ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from keras.layers import Conv2D, Add, ZeroPadding2D, UpSampling2D, Concatenate
from keras.layers import Input, Lambda
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.models import load_model
from keras.optimizers import Adadelta
from keras.regularizers import l2
from keras.utils import multi_gpu_model
from matplotlib.colors import rgb_to_hsv, hsv_to_rgb

MAX_VERTICES = 4


def compose(*funcs):
    """Compose arbitrarily many functions, evaluated left to right.

    Reference: https://mathieularose.com/function-composition-in-python/
    """
    # return lambda x: reduce(lambda v, f: f(v), funcs, x)
    if funcs:
        return reduce(lambda f, g: lambda *a, **kw: g(f(*a, **kw)), funcs)
    else:
        raise ValueError('Composition of empty sequence not supported.')


def letterbox_image(image, size):
    '''resize image with unchanged aspect ratio using padding'''
    iw, ih = image.size
    w, h = size
    scale = min(w / iw, h / ih)
    nw = int(iw * scale)
    nh = int(ih * scale)

    image = image.resize((nw, nh), Image.BICUBIC)
    new_image = Image.new('RGB', size, (128, 128, 128))
    new_image.paste(image, ((w - nw) // 2, (h - nh) // 2))
    return new_image


def rand(a=0, b=1):
    return np.random.rand() * (b - a) + a


# !!! changelog !!!
# sat 2.9 instead of 1.9
# val 3.3 instead of 1.9
# lanczos instead bicubic
# added noise
# added rotation
def get_random_data(annotation_line, input_shape, random=False, max_boxes=250, jitter=.3, hue=.15, sat=2.9, val=3.3,
                    proc_img=True):
    '''random preprocessing for real-time data augmentation'''
    line = annotation_line.split()

    #for box in line[1:]:
    #    if len(box.split(',')) != 13:
    #        print(box)
    #print(annotation_line)

    for element in range(1, len(line)):
        for symbol in range(line[element].count(',') - 4, MAX_VERTICES * 2):
            line[element] = line[element] + ',0'
        # totok

    image = Image.open(line[0])
    iw, ih = image.size
    h, w = input_shape
    # rozseka radek v label textaku prve na boxy a potom samotne hodnoty boxu podle carky
    box = np.array([np.array(list(map(int, box.split(',')))) for box in line[1:]])

    #change ordering of points to start with top left point
    '''
    tmp0 = box[:, 5:7].copy()
    tmp1 = box[:, 7:9].copy()
    tmp2 = box[:, 9:11].copy()
    tmp3 = box[:, 11:13].copy()
    box[:, 5:7] = tmp1
    box[:, 7:9] = tmp2
    box[:, 9:11] = tmp3
    box[:, 11:13] = tmp0
    '''


    # resize image
    scale = min(w / iw, h / ih)
    nw = int(iw * scale)
    nh = int(ih * scale)
    dx = (w - nw) // 2
    dy = (h - nh) // 2
    image_data = 0
    if proc_img:
        image = image.resize((nw, nh), Image.LANCZOS)
        new_image = Image.new('RGB', (w, h), (128, 128, 128))
        new_image.paste(image, (dx, dy))
        image = new_image
        image_data = np.array(new_image) / 255.

    if not random:
        # correct boxes
        box_data = np.zeros((max_boxes, 5 + MAX_VERTICES * 2))
        if len(box) > 0:
            # nahodne zamicha boxy
            np.random.shuffle(box)
            # pokud labely obsahovaly vetsi pocet boxu nez je povoleno, tak vezmi jen povoleny pocet (max_boxes)
            if len(box) > max_boxes: box = box[:max_boxes]
            box[:, [0, 2]] = box[:, [0, 2]] * scale + dx
            box[:, [1, 3]] = box[:, [1, 3]] * scale + dy
            for i in range(5, 5+MAX_VERTICES * 2, 2):
                box[:, i]     = box[:, i]   * scale + dx
                box[:, i + 1] = box[:, i+1] * scale + dy



            box_data[:len(box)] = box

        return image_data, box_data


    '''
    # horizontal image flipping
    flip_h = rand() < .5
    if flip_h:
        image = image.transpose(Image.FLIP_LEFT_RIGHT)
        if len(box) > 0:
            box[:, [0, 2]] = (w - 1) - box[:, [0, 2]]
            for i in range(5, 5+MAX_VERTICES * 2, 2):
                box[:, i] = (w - 1) - box[:, i]
                box[:, i][box[:, i] == w] = 0

    # vertical image flipping
    flip_v = rand() < .5
    if flip_v:
        image = image.transpose(Image.FLIP_TOP_BOTTOM)
        if len(box) > 0:
            box[:, [1, 3]] = (h - 1) - box[:, [1, 3]]
            for i in range(6, 5+MAX_VERTICES * 2, 2):
                box[:, i] = (h - 1) - box[:, i]
                box[:, i][box[:, i] == h] = 0
    '''
    # med blur to homogenize image
    if np.random.rand() < .6:
        if np.random.rand() < .5:
            image = cv.medianBlur(np.array(image), 3)
        else:
            image = cv.medianBlur(np.array(image), 5)

    # distort hsv image
    hue = rand(-hue, hue)
    sat = rand(1, sat) if rand() < .5 else 1 / rand(1, sat)
    val = rand(1, val) if rand() < .5 else 1 / rand(1, val)
    x = rgb_to_hsv(np.array(image) / 255.)
    x[..., 0] += hue
    x[..., 0][x[..., 0] > 1] -= 1
    x[..., 0][x[..., 0] < 0] += 1
    x[..., 1] *= sat
    x[..., 2] *= val
    x[x > 1] = 1
    x[x < 0] = 0
    image_data = hsv_to_rgb(x)  # numpy array, 0 to 1

    # add noise
    if np.random.rand() < .5:
        image_data = np.clip(image_data + np.random.rand() * image_data.std() * np.random.random(image_data.shape), 0, 1)


    box_data = np.zeros((max_boxes, 5 + MAX_VERTICES*2))
    if len(box) > 0:
        # nahodne zamicha boxy
        np.random.shuffle(box)
        # pokud labely obsahovaly vetsi pocet boxu nez je povoleno, tak vezmi jen povoleny pocet (max_boxes)
        if len(box) > max_boxes: box = box[:max_boxes]
        box[:, [0, 2]] = box[:, [0, 2]] * scale + dx
        box[:, [1, 3]] = box[:, [1, 3]] * scale + dy
        for i in range(5, 5 + MAX_VERTICES * 2, 2):
            box[:, i] = box[:, i] * scale + dx
            box[:, i + 1] = box[:, i + 1] * scale + dy
        box_data[:len(box)] = box


    return image_data, box_data


"""YOLO_v3 Model Defined in Keras."""


@wraps(Conv2D)
def DarknetConv2D(*args, **kwargs):
    """Wrapper to set Darknet parameters for Convolution2D."""
    darknet_conv_kwargs = {'kernel_regularizer': l2(5e-4)}
    darknet_conv_kwargs['padding'] = 'valid' if kwargs.get('strides') == (2, 2) else 'same'
    darknet_conv_kwargs.update(kwargs)
    return Conv2D(*args, **darknet_conv_kwargs)


def DarknetConv2D_BN_Leaky(*args, **kwargs):
    """Darknet Convolution2D followed by BatchNormalization and LeakyReLU."""
    no_bias_kwargs = {'use_bias': False}
    no_bias_kwargs.update(kwargs)
    return compose(
        DarknetConv2D(*args, **no_bias_kwargs),
        BatchNormalization(),
        LeakyReLU(alpha=0.1))


def resblock_body(x, num_filters, num_blocks):
    '''A series of resblocks starting with a downsampling Convolution2D'''
    # Darknet uses left and top padding instead of 'same' mode
    x = ZeroPadding2D(((1, 0), (1, 0)))(x)
    x = DarknetConv2D_BN_Leaky(num_filters, (3, 3), strides=(2, 2))(x)
    for i in range(num_blocks):
        y = compose(
            DarknetConv2D_BN_Leaky(num_filters // 2, (1, 1)),
            DarknetConv2D_BN_Leaky(num_filters, (3, 3)))(x)
        x = Add()([x, y])
    return x


def darknet_body(x):
    '''Darknent body having 52 Convolution2D layers'''
    base = 8  # orig base = 8
    x = DarknetConv2D_BN_Leaky(base * 4, (3, 3))(x)
    x = resblock_body(x, base * 8, 1)
    x = resblock_body(x, base * 16, 2)
    x = resblock_body(x, base * 32, 8)
    x = resblock_body(x, base * 64, 8)
    x = resblock_body(x, base * 128, 4)
    return x


def make_last_layers(x, num_filters, out_filters):
    '''6 Conv2D_BN_Leaky layers followed by a Conv2D_linear layer'''
    x = compose(
        DarknetConv2D_BN_Leaky(num_filters, (1, 1)),
        DarknetConv2D_BN_Leaky(num_filters * 2, (3, 3)),
        DarknetConv2D_BN_Leaky(num_filters, (1, 1)),
        DarknetConv2D_BN_Leaky(num_filters * 2, (3, 3)),
        DarknetConv2D_BN_Leaky(num_filters, (1, 1)))(x)
    y = compose(
        DarknetConv2D_BN_Leaky(num_filters * 2, (3, 3)),
        DarknetConv2D(out_filters, (1, 1)))(x)
    return x, y


def yolo_body(inputs, num_anchors, num_classes):
    """Create YOLO_V3 model CNN body in Keras."""
    darknet = Model(inputs, darknet_body(inputs))
    x, y1 = make_last_layers(darknet.output, 512, num_anchors * (num_classes + 5 + MAX_VERTICES * 2))  # 512

    x = compose(
        DarknetConv2D_BN_Leaky(256, (1, 1)),  # 256
        UpSampling2D(2))(x)
    x = Concatenate()([x, darknet.layers[152].output])
    x, y2 = make_last_layers(x, 256, num_anchors * (num_classes + 5 + MAX_VERTICES * 2))

    x = compose(
        DarknetConv2D_BN_Leaky(128, (1, 1)),  # 128
        UpSampling2D(2))(x)
    x = Concatenate()([x, darknet.layers[92].output])
    x, y3 = make_last_layers(x, 128, num_anchors * (num_classes + 5 + MAX_VERTICES * 2))  # 69

    return Model(inputs, [y1, y2, y3])


def yolo_head(feats, anchors, num_classes, input_shape, calc_loss=False):
    """Convert final layer features to bounding box parameters."""
    num_anchors = len(anchors)
    # Reshape to batch, height, width, num_anchors, box_params.
    anchors_tensor = K.reshape(K.constant(anchors), [1, 1, 1, num_anchors, 2])

    grid_shape = K.shape(feats)[1:3]  # height, width
    grid_y = K.tile(K.reshape(K.arange(0, stop=grid_shape[0]), [-1, 1, 1, 1]),
                    [1, grid_shape[1], 1, 1])
    grid_x = K.tile(K.reshape(K.arange(0, stop=grid_shape[1]), [1, -1, 1, 1]),
                    [grid_shape[0], 1, 1, 1])
    grid = K.concatenate([grid_x, grid_y])
    grid = K.cast(grid, K.dtype(feats))

    feats = K.reshape(
        feats, [-1, grid_shape[0], grid_shape[1], num_anchors, num_classes + 5 + MAX_VERTICES * 2])

    # Adjust preditions to each spatial grid point and anchor size.
    box_xy = (K.sigmoid(feats[..., :2]) + grid) / K.cast(grid_shape[::-1], K.dtype(feats))
    box_wh = K.exp(feats[..., 2:4]) * anchors_tensor / K.cast(input_shape[::-1], K.dtype(feats))
    box_confidence = K.sigmoid(feats[..., 4:5])
    box_class_probs = K.sigmoid(feats[..., 5:5 + num_classes])

    # zapsani vrcholu polygonu
    polygons_x = feats[..., 5 + num_classes:num_classes + 5 + MAX_VERTICES * 2:2]  * anchors_tensor[..., 0:1] / K.cast(input_shape[::-1][0], K.dtype(feats))
    polygons_y = feats[..., 5 + num_classes + 1:num_classes + 5 + MAX_VERTICES * 2:2]  * anchors_tensor[..., 1:2] / K.cast(input_shape[::-1][1], K.dtype(feats))

    #raw_true_polygon_x = y_true[l][..., 5 + num_classes:5 + num_classes + MAX_VERTICES * 2:2] / anchors[anchor_mask[l]][:, 0:1] * input_shape[::-1][0]

    if calc_loss == True:
        return grid, feats, box_xy, box_wh
    return box_xy, box_wh, box_confidence, box_class_probs, polygons_x, polygons_y


def yolo_correct_boxes(box_xy, box_wh, input_shape, image_shape):
    '''Get corrected boxes'''
    box_yx = box_xy[..., ::-1]
    box_hw = box_wh[..., ::-1]
    input_shape = K.cast(input_shape, K.dtype(box_yx))
    image_shape = K.cast(image_shape, K.dtype(box_yx))
    new_shape = K.round(image_shape * K.min(input_shape / image_shape))
    offset = (input_shape - new_shape) / 2. / input_shape
    scale = input_shape / new_shape
    box_yx = (box_yx - offset) * scale
    box_hw *= scale

    box_mins = box_yx - (box_hw / 2.)
    box_maxes = box_yx + (box_hw / 2.)
    boxes = K.concatenate([
        box_mins[..., 0:1],  # y_min
        box_mins[..., 1:2],  # x_min
        box_maxes[..., 0:1],  # y_max
        box_maxes[..., 1:2]  # x_max
    ])

    # Scale boxes back to original image shape.
    boxes *= K.concatenate([image_shape, image_shape])
    return boxes


def yolo_correct_polygons(polygons_x, polygons_y, input_shape, image_shape):
    '''Get corrected boxes'''
    polygons_x = polygons_x[..., ::-1]
    polygons_y = polygons_y[..., ::-1]

    input_shape = K.cast(input_shape, K.dtype(polygons_x))
    image_shape = K.cast(image_shape, K.dtype(polygons_x))
    new_shape = K.round(image_shape * K.min(input_shape / image_shape))
    scale = input_shape / new_shape
    polygons_x *= scale[0]
    polygons_y *= scale[1]

    polygon_x = polygons_x * image_shape[0]
    polygon_y = polygons_y * image_shape[1]
    polygons = K.concatenate([polygon_x, polygon_y])
    return polygons


def yolo_boxes_and_scores(feats, anchors, num_classes, input_shape, image_shape):
    '''Process Conv layer output'''
    box_xy, box_wh, box_confidence, box_class_probs, polygons_x, polygons_y = yolo_head(feats,
                                                                                        anchors, num_classes,
                                                                                        input_shape)
    boxes = yolo_correct_boxes(box_xy, box_wh, input_shape, image_shape)
    boxes = K.reshape(boxes, [-1, 4])
    box_scores = box_confidence * box_class_probs
    box_scores = K.reshape(box_scores, [-1, num_classes])
    polygons = yolo_correct_polygons(polygons_x, polygons_y, input_shape, image_shape)
    polygons = K.reshape(polygons, [-1, MAX_VERTICES * 2])
    return boxes, box_scores, polygons


def yolo_eval(yolo_outputs,
              anchors,
              num_classes,
              image_shape,
              max_boxes=250,
              score_threshold=.4,
              iou_threshold=.5):
    """Evaluate YOLO model on given input and return filtered boxes."""
    num_layers = len(yolo_outputs)
    anchor_mask = [[6, 7, 8], [3, 4, 5], [0, 1, 2]] if num_layers == 3 else [[3, 4, 5], [1, 2, 3]]  # default setting
    input_shape = K.shape(yolo_outputs[0])[1:3] * 32
    boxes = []
    box_scores = []
    polygons = []

    for l in range(num_layers):
        _boxes, _box_scores, _polygons = yolo_boxes_and_scores(yolo_outputs[l],
                                                               anchors[anchor_mask[l]], num_classes, input_shape,
                                                               image_shape)
        boxes.append(_boxes)
        box_scores.append(_box_scores)
        polygons.append(_polygons)
    boxes = K.concatenate(boxes, axis=0)
    box_scores = K.concatenate(box_scores, axis=0)
    polygons = K.concatenate(polygons, axis=0)

    mask = box_scores >= score_threshold
    max_boxes_tensor = K.constant(max_boxes, dtype='int32')
    boxes_ = []
    scores_ = []
    classes_ = []
    polygons_ = []
    for c in range(num_classes):
        # TODO: use keras backend instead of tf.
        class_boxes = tf.boolean_mask(boxes, mask[:, c])
        class_polygons = tf.boolean_mask(polygons, mask[:, c])
        class_box_scores = tf.boolean_mask(box_scores[:, c], mask[:, c])
        nms_index = tf.image.non_max_suppression(
            class_boxes, class_box_scores, max_boxes_tensor, iou_threshold=iou_threshold)
        class_boxes = K.gather(class_boxes, nms_index)
        class_box_scores = K.gather(class_box_scores, nms_index)
        class_polygons = K.gather(class_polygons, nms_index)
        classes = K.ones_like(class_box_scores, 'int32') * c
        boxes_.append(class_boxes)
        scores_.append(class_box_scores)
        classes_.append(classes)
        polygons_.append(class_polygons)
    polygons_ = K.concatenate(polygons_, axis=0)
    boxes_ = K.concatenate(boxes_, axis=0)
    scores_ = K.concatenate(scores_, axis=0)
    classes_ = K.concatenate(classes_, axis=0)

    return boxes_, scores_, classes_, polygons_


def preprocess_true_boxes(true_boxes, input_shape, anchors, num_classes):
    '''Preprocess true boxes to training input format

    Parameters
    ----------
    true_boxes: array, shape=(m, T, 5+69)
        Absolute x_min, y_min, x_max, y_max, class_id relative to input_shape.
    input_shape: array-like, hw, multiples of 32
    anchors: array, shape=(N, 2), wh
    num_classes: integer

    Returns
    -------
    y_true: list of array, shape like yolo_outputs, xywh are reletive value

    '''
    assert (true_boxes[..., 4] < num_classes).all(), 'class id must be less than num_classes'
    num_layers = len(anchors) // 3  # default setting
    anchor_mask = [[6, 7, 8], [3, 4, 5], [0, 1, 2]] if num_layers == 3 else [[3, 4, 5], [1, 2, 3]]
    # shape = [2, 25, 85] == [batch, max_boxes, delka vektoru] - tady je asi problem -> u kazdeho z 25 boxu mame ten polygon, tedy 25x duplicita
    true_boxes = np.array(true_boxes, dtype='float32')
    input_shape = np.array(input_shape, dtype='int32')
    boxes_xy = (true_boxes[..., 0:2] + true_boxes[..., 2:4]) // 2 #totok je stred
    boxes_wh = true_boxes[..., 2:4] - true_boxes[..., 0:2]   #sirka a vyska boxu
    true_boxes[..., 0:2] = boxes_xy / input_shape[::-1]
    true_boxes[..., 2:4] = boxes_wh / input_shape[::-1]
    # vydeli vrcholy polygonu velikosti obrazku -> relativni souradnice
    for i in range(5, 5+MAX_VERTICES * 2, 2):
        #nova x,y souradnice bodu je top left boxu - pudovodni xy + polovina sirky a vysky
        true_boxes[..., i:i + 2] = true_boxes[..., 0:2] - (true_boxes[..., i:i+2]/ input_shape[::-1]) + (true_boxes[..., 2:4] / 2.0)

    true_boxes = np.clip(true_boxes, 0, 1)

    m = true_boxes.shape[0]
    grid_shapes = [input_shape // {0: 32, 1: 16, 2: 8}[l] for l in range(num_layers)]
    y_true = [
        np.zeros((m, grid_shapes[l][0], grid_shapes[l][1], len(anchor_mask[l]), 5 + num_classes + MAX_VERTICES * 2),
                 dtype='float32') for l in range(num_layers)]

    # Expand dim to apply broadcasting.
    anchors = np.expand_dims(anchors, 0)
    anchor_maxes = anchors / 2.
    anchor_mins = -anchor_maxes
    valid_mask = boxes_wh[..., 0] > 0
    # pro vsechny boxy
    for b in range(m):
        # Discard zero rows.
        wh = boxes_wh[b, valid_mask[b]]
        if len(wh) == 0: continue
        # Expand dim to apply broadcasting.
        wh = np.expand_dims(wh, -2)
        box_maxes = wh / 2.
        box_mins = -box_maxes

        intersect_mins = np.maximum(box_mins, anchor_mins)
        intersect_maxes = np.minimum(box_maxes, anchor_maxes)
        intersect_wh = np.maximum(intersect_maxes - intersect_mins, 0.)
        intersect_area = intersect_wh[..., 0] * intersect_wh[..., 1]
        box_area = wh[..., 0] * wh[..., 1]
        anchor_area = anchors[..., 0] * anchors[..., 1]
        iou = intersect_area / (box_area + anchor_area - intersect_area)

        # Find best anchor for each true box
        best_anchor = np.argmax(iou, axis=-1)
        for t, n in enumerate(best_anchor):
            for l in range(num_layers):
                if n in anchor_mask[l]:
                    i = np.floor(true_boxes[b, t, 0] * grid_shapes[l][1]).astype('int32')
                    j = np.floor(true_boxes[b, t, 1] * grid_shapes[l][0]).astype('int32')
                    k = anchor_mask[l].index(n)
                    c = true_boxes[b, t, 4].astype('int32')
                    y_true[l][b, j, i, k, 0:4] = true_boxes[b, t, 0:4]
                    y_true[l][b, j, i, k, 4] = 1
                    y_true[l][b, j, i, k, 5 + c] = 1
                    y_true[l][b, j, i, k, 5 + num_classes:5 + num_classes + MAX_VERTICES * 2] = \
                        true_boxes[b, t, 5: 5 + MAX_VERTICES * 2]

    return y_true


def box_iou(b1, b2):
    """Return iou tensor

    Parameters
    ----------
    b1: tensor, shape=(i1,...,iN, 4), xywh
    b2: tensor, shape=(j, 4), xywh

    Returns
    -------
    iou: tensor, shape=(i1,...,iN, j)

    """

    # Expand dim to apply broadcasting.
    b1 = K.expand_dims(b1, -2)
    b1_xy = b1[..., :2]
    b1_wh = b1[..., 2:4]
    b1_wh_half = b1_wh / 2.
    b1_mins = b1_xy - b1_wh_half
    b1_maxes = b1_xy + b1_wh_half

    # Expand dim to apply broadcasting.
    b2 = K.expand_dims(b2, 0)
    b2_xy = b2[..., :2]
    b2_wh = b2[..., 2:4]
    b2_wh_half = b2_wh / 2.
    b2_mins = b2_xy - b2_wh_half
    b2_maxes = b2_xy + b2_wh_half

    intersect_mins = K.maximum(b1_mins, b2_mins)
    intersect_maxes = K.minimum(b1_maxes, b2_maxes)
    intersect_wh = K.maximum(intersect_maxes - intersect_mins, 0.)
    intersect_area = intersect_wh[..., 0] * intersect_wh[..., 1]
    b1_area = b1_wh[..., 0] * b1_wh[..., 1]
    b2_area = b2_wh[..., 0] * b2_wh[..., 1]
    iou = intersect_area / (b1_area + b2_area - intersect_area)

    return iou


def yolo_loss(args, anchors, num_classes, ignore_thresh=.5, print_loss=False):
    """Return yolo_loss tensor

    Parameters
    ----------
    yolo_outputs: list of tensor, the output of yolo_body or tiny_yolo_body
    y_true: list of array, the output of preprocess_true_boxes
    anchors: array, shape=(N, 2), wh
    num_classes: integer
    ignore_thresh: float, the iou threshold whether to ignore object confidence loss

    Returns
    -------
    loss: tensor, shape=(1,)

    """
    num_layers = len(anchors) // 3  # default setting
    yolo_outputs = args[:num_layers]
    y_true = args[num_layers:]
    global g_true
    g_true = y_true
    anchor_mask = [[6, 7, 8], [3, 4, 5], [0, 1, 2]] if num_layers == 3 else [[3, 4, 5], [1, 2, 3]]
    input_shape = K.cast(K.shape(yolo_outputs[0])[1:3] * 32, K.dtype(y_true[0]))
    grid_shapes = [K.cast(K.shape(yolo_outputs[l])[1:3], K.dtype(y_true[0])) for l in range(num_layers)]
    loss = 0
    m = K.shape(yolo_outputs[0])[0]  # batch size, tensor
    mf = K.cast(m, K.dtype(yolo_outputs[0]))

    for l in range(num_layers):
        object_mask = y_true[l][..., 4:5]
        true_class_probs = y_true[l][..., 5:5 + num_classes]

        grid, raw_pred, pred_xy, pred_wh = yolo_head(yolo_outputs[l], anchors[anchor_mask[l]], num_classes, input_shape, calc_loss=True)
        pred_box = K.concatenate([pred_xy, pred_wh])
        # grid_shape je list ktery drzi dimenze mrizky pro vsechna rozliseni, kazdy prvek listu je numpy array o dvou prvcich
        if l == 1:
            global g_grid_s
            g_grid_s = grid_shapes
        # grid jsou indexy vsech bunek (0,1) (0,2) (0,3) ... (51, 51) atd.
        if l == 1:
            global g_grid
            g_grid = grid
        # Darknet raw box to calculate loss.
        # stred boxu z [0, 1] * [velikost gridu] - [index bunky v gridu]
        raw_true_xy = y_true[l][..., :2] * grid_shapes[l][::-1] - grid
        if l == 1:
            global g_r_xy
            g_r_xy = raw_true_xy
        raw_true_wh = K.log(y_true[l][..., 2:4] / anchors[anchor_mask[l]] * input_shape[::-1])
        raw_true_wh = K.switch(object_mask, raw_true_wh, K.zeros_like(raw_true_wh))  # avoid log(0)=-inf

        #raw_true_polygon_x = K.log(y_true[l][..., 5 + num_classes:5 + num_classes + MAX_VERTICES * 2:2] / anchors[anchor_mask[l]][:, 0:1] *input_shape[::-1][0] + K.epsilon())
        #raw_true_polygon_x = K.switch(object_mask, raw_true_polygon_x, K.zeros_like(raw_true_polygon_x))
        #raw_true_polygon_y = K.log(y_true[l][..., 5 + num_classes + 1:5 + num_classes + MAX_VERTICES * 2:2] / anchors[anchor_mask[l]][:, 1:2] *input_shape[::-1][1] + K.epsilon())
        #raw_true_polygon_y = K.switch(object_mask, raw_true_polygon_y, K.zeros_like(raw_true_polygon_y))

        raw_true_polygon_x = y_true[l][..., 5 + num_classes:5 + num_classes + MAX_VERTICES * 2:2] / anchors[anchor_mask[l]][:, 0:1] *input_shape[::-1][0]
        raw_true_polygon_y = y_true[l][..., 5 + num_classes+1:5 + num_classes + MAX_VERTICES * 2:2] / anchors[anchor_mask[l]][:, 1:2] *input_shape[::-1][1]


        # z toho logu lezou nejake zaporne hodnoty
        if l == 1:
            global g_wh
            g_wh = raw_true_wh
        box_loss_scale = 2 - y_true[l][..., 2:3] * y_true[l][..., 3:4]
        if l == 1:
            global g_blc
            g_blc = box_loss_scale
        # nacte vsechny vrcholy polygonu


        # Find ignore mask, iterate over each of batch.
        ignore_mask = tf.TensorArray(K.dtype(y_true[0]), size=1, dynamic_size=True)
        object_mask_bool = K.cast(object_mask, 'bool')

        def loop_body(b, ignore_mask):
            true_box = tf.boolean_mask(y_true[l][b, ..., 0:4], object_mask_bool[b, ..., 0])
            iou = box_iou(pred_box[b], true_box)
            best_iou = K.max(iou, axis=-1)
            ignore_mask = ignore_mask.write(b, K.cast(best_iou < ignore_thresh, K.dtype(true_box)))
            return b + 1, ignore_mask

        _, ignore_mask = K.control_flow_ops.while_loop(lambda b, *args: b < m, loop_body, [0, ignore_mask])
        ignore_mask = ignore_mask.stack()
        ignore_mask = K.expand_dims(ignore_mask, -1)

        # K.binary_crossentropy is helpful to avoid exp overflow.
        xy_loss = object_mask * box_loss_scale * K.binary_crossentropy(raw_true_xy, raw_pred[..., 0:2],from_logits=True)
        wh_loss = object_mask * box_loss_scale * 0.5 * K.square(raw_true_wh - raw_pred[..., 2:4])
        confidence_loss = object_mask * K.binary_crossentropy(object_mask, raw_pred[..., 4:5], from_logits=True) + (1 - object_mask) * K.binary_crossentropy(object_mask, raw_pred[..., 4:5], from_logits=True) * ignore_mask
        class_loss = object_mask * K.binary_crossentropy(true_class_probs, raw_pred[..., 5:5 + num_classes],from_logits=True)

        polygon_loss_x = object_mask * box_loss_scale * 0.5 * K.square(raw_true_polygon_x - raw_pred[..., 5 + num_classes:5 + num_classes + MAX_VERTICES * 2:2])
        polygon_loss_y = object_mask * box_loss_scale * 0.5 * K.square(raw_true_polygon_y - raw_pred[..., 5 + num_classes + 1:5 + num_classes + MAX_VERTICES * 2:2])

        #polygon_loss_x = object_mask * box_loss_scale * K.binary_crossentropy(raw_true_polygon_x, raw_pred[..., 5 + num_classes:5 + num_classes + MAX_VERTICES * 2:2], from_logits=True)
        #polygon_loss_y = object_mask * box_loss_scale * K.binary_crossentropy(raw_true_polygon_y, raw_pred[..., 5 + num_classes + 1:5 + num_classes + MAX_VERTICES * 2:2], from_logits=True)

        xy_loss = K.sum(xy_loss) / mf
        wh_loss = K.sum(wh_loss) / mf

        '''object_mask * box_loss_scale *'''
        global g_pred
        global g_poly
        global g_poly_x
        if l == 1:
            g_pred = raw_pred
            g_poly_x = raw_true_polygon_x
         #   g_poly = raw_true_polygon0



        confidence_loss = K.sum(confidence_loss) / mf
        class_loss = K.sum(class_loss) / mf
        polygon_loss = (K.sum(polygon_loss_x) + K.sum(polygon_loss_y)) / mf

        loss += xy_loss + wh_loss + confidence_loss + class_loss + polygon_loss
    return loss


class YOLO(object):
    _defaults = {
        "model_path": 'model_data/yolo.h5',
        "anchors_path": 'yolo_anchors.txt',
        "classes_path": 'yolo_classes.txt',
        "score": 0.5,
        "iou": 0.5,
        "model_image_size": (416, 416),
        "gpu_num": 1,
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls._defaults[n]
        else:
            return "Unrecognized attribute name '" + n + "'"

    def __init__(self, **kwargs):
        self.__dict__.update(self._defaults)  # set up default values
        self.__dict__.update(kwargs)  # and update with user overrides
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = K.get_session()
        self.boxes, self.scores, self.classes, self.polygons = self.generate()

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file.'

        # Load model, or construct model and load weights.
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        is_tiny_version = num_anchors == 6  # default setting
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except:
            self.yolo_model = yolo_body(Input(shape=(None, None, 3)), num_anchors // 3, num_classes)
            self.yolo_model.load_weights(self.model_path)  # make sure model, anchors and classes match
        else:
            # novy output
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                   num_anchors / len(self.yolo_model.output) * (num_classes + 5 + MAX_VERTICES * 2), \
                'Mismatch between model and given anchor and class sizes'

        print('{} model, anchors, and classes loaded.'.format(model_path))

        # Generate colors for drawing bounding boxes.
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))
        np.random.seed(10101)  # Fixed seed for consistent colors across runs.
        np.random.shuffle(self.colors)  # Shuffle colors to decorrelate adjacent classes.
        np.random.seed(None)  # Reset seed to default.

        # Generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2,))
        if self.gpu_num >= 2:
            self.yolo_model = multi_gpu_model(self.yolo_model, gpus=self.gpu_num)
        boxes, scores, classes, polygons = yolo_eval(self.yolo_model.output, self.anchors,
                                                     len(self.class_names), self.input_image_shape,
                                                     score_threshold=self.score, iou_threshold=self.iou)
        return boxes, scores, classes, polygons

    def detect_image(self, image):
        # start = timer()

        if self.model_image_size != (None, None):
            assert self.model_image_size[0] % 32 == 0, 'Multiples of 32 required'
            assert self.model_image_size[1] % 32 == 0, 'Multiples of 32 required'
            boxed_image = letterbox_image(image, tuple(reversed(self.model_image_size)))
        else:
            new_image_size = (image.width - (image.width % 32),
                              image.height - (image.height % 32))
            boxed_image = letterbox_image(image, new_image_size)
        image_data = np.array(boxed_image, dtype='float32')

        # print(image_data.shape)
        image_data /= 255.
        image_data = np.expand_dims(image_data, 0)  # Add batch dimension.
        #print('image data shape', image_data.shape)
        #print('image size', image.size)
        out_boxes, out_scores, out_classes, polygons = self.sess.run(
            [self.boxes, self.scores, self.classes, self.polygons],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: [image.size[1], image.size[0]],
                K.learning_phase(): 0
            })

        return out_boxes, out_scores, out_classes, polygons

    def close_session(self):
        self.sess.close()


if __name__ == "__main__":

    """
    Retrain the YOLO model for your own dataset.
    """
    

    def _main():
        phase = 2
        annotation_path = 'data-for-yolo-training.txt'
        log_dir = 'detector/'
        classes_path = 'yolo_classes.txt'
        anchors_path = 'yolo_anchors.txt'
        class_names = get_classes(classes_path)
        num_classes = len(class_names)
        anchors = get_anchors(anchors_path)

        input_shape = (416,416) # multiple of 32, hw

        if phase == 1:
            model = create_model(input_shape, anchors, num_classes, load_pretrained=False)
        else:
            model = create_model(input_shape, anchors, num_classes, load_pretrained=True, weights_path='yolo-wh-size.h5')

        logging = TensorBoard(log_dir=log_dir)
        checkpoint = ModelCheckpoint(log_dir + 'ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5',
            monitor='val_loss', save_weights_only=True, save_best_only=True, period=1, verbose=1)
        reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.5, patience=2, verbose=1, delta=0.03)
        early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=1)

        val_split = 0.1
        with open(annotation_path) as f:
            lines = f.readlines()
        np.random.seed(1)
        np.random.shuffle(lines)
        np.random.seed(None)
        num_val = int(len(lines)*val_split)
        num_train = len(lines) - num_val


        if phase == 1:
            model.compile(optimizer=Adadelta(lr=1.0), loss={'yolo_loss': lambda y_true, y_pred: y_pred})
            epochs = 100
        elif phase == 2:
            model.compile(optimizer=Adadelta(lr=1.0), loss={'yolo_loss': lambda y_true, y_pred: y_pred}) #phase 2
            epochs = 100
        elif phase == 3:
            model.compile(optimizer=Adadelta(lr=0.25), loss={'yolo_loss': lambda y_true, y_pred: y_pred}) #phase 3
            epochs = 500

        batch_size = 8 # note that more GPU memory is required after unfreezing the body
        print('Train on {} samples, val on {} samples, with batch size {}.'.format(num_train, num_val, batch_size))
        from numpy.random import seed
        from tensorflow import set_random_seed
        np.random.seed(1)
        seed(1)
        set_random_seed(1)

                                                                                                                              
        model.fit_generator(data_generator_wrapper(lines[:num_train], batch_size, input_shape, anchors, num_classes),
            steps_per_epoch=max(1, num_train//batch_size)*3,
            validation_data=data_generator_wrapper(lines[num_train:], batch_size, input_shape, anchors, num_classes),
            validation_steps=max(1, num_val//batch_size),
            epochs=epochs,
            initial_epoch=0,
            callbacks=[reduce_lr, checkpoint, early_stopping])
        '''
        model.fit_generator(data_generator_wrapper(lines, batch_size, input_shape, anchors, num_classes),
                            steps_per_epoch=max(1, num_train // batch_size),
                            validation_data=data_generator_wrapper(lines, batch_size, input_shape, anchors, num_classes),
                            validation_steps=max(1, num_val // batch_size),
                            epochs=epochs,
                            initial_epoch=0,
                            callbacks=[reduce_lr, checkpoint, early_stopping])
        '''                            
        model.save_weights(log_dir + 'trained_weights_final.h5')


    def get_classes(classes_path):
        """loads the classes"""
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names


    def get_anchors(anchors_path):
        """loads the anchors from a file"""
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)


    def create_model(input_shape, anchors, num_classes, load_pretrained=True, freeze_body=2,
                     weights_path='model_data/yolo_weights.h5'):
        """create the training model"""
        K.clear_session()  # get a new session
        image_input = Input(shape=(None, None, 3))
        h, w = input_shape
        num_anchors = len(anchors)
        y_true = [Input(shape=(h // {0: 32, 1: 16, 2: 8}[l], w // {0: 32, 1: 16, 2: 8}[l], \
                               num_anchors // 3, num_classes + 5 + MAX_VERTICES * 2)) for l in range(3)]

        model_body = yolo_body(image_input, num_anchors // 3, num_classes)
        print('Create YOLOv3 model with {} anchors and {} classes.'.format(num_anchors, num_classes))

        if load_pretrained:
            model_body.load_weights(weights_path, by_name=True, skip_mismatch=True)
            print('Load weights {}.'.format(weights_path))

        model_loss = Lambda(yolo_loss, output_shape=(1,), name='yolo_loss',
                            arguments={'anchors': anchors, 'num_classes': num_classes, 'ignore_thresh': 0.5})(
            [*model_body.output, *y_true])
        model = Model([model_body.input, *y_true], model_loss)

        #print(model.summary())
        return model


    def data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes):
        """data generator for fit_generator"""
        n = len(annotation_lines)
        i = 0
        while True:
            image_data = []
            box_data = []
            for b in range(batch_size):
                if i == 0:
                    np.random.shuffle(annotation_lines)
                # image, box = get_random_data(annotation_lines[i], input_shape, random=False)
                # turned off preprocessing because image will be disorted
                image, box = get_random_data(annotation_lines[i], input_shape, random=True, jitter=0, hue=0)
                image_data.append(image)
                box_data.append(box)
                i = (i + 1) % n
            image_data = np.array(image_data)
            box_data = np.array(box_data)
            y_true = preprocess_true_boxes(box_data, input_shape, anchors, num_classes)
            yield [image_data, *y_true], np.zeros(batch_size)


    def data_generator_wrapper(annotation_lines, batch_size, input_shape, anchors, num_classes):
        n = len(annotation_lines)
        if n == 0 or batch_size <= 0: return None
        return data_generator(annotation_lines, batch_size, input_shape, anchors, num_classes)


    if __name__ == '__main__':
        _main()
